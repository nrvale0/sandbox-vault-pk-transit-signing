#!/bin/bash

set -eux

: ${DEFAULT_VAULT_ROOT_TOKEN:=password}
: ${VAULT_ADDR:=http://localhost:8200}

: ${ORG_CN:=myorg.example}

#
# Fire up a backgrounded dev-mode Vault server which will host the
# PKI and Transit Secret Engines.
#
(vault server -dev -dev-root-token-id=${DEFAULT_VAULT_ROOT_TOKEN}) &
disown

#
# Give Vault a chance to settle before we start the fun.
#
sleep 2

#
# Enable pki engine and set the lifespan to 1 year.
# 
vault secrets enable pki
vault secrets tune -max-lease-ttl=8760h pki

#
# Generate the CA which will be used to sign the license-signing key later
# exported from Transit. For demo purposes this CA is a self-signed root
# CA however with a few minor adjustments to the read/write paths below
# this could just as easily be an intermediate CA signed by an offline
# or air-gapped CA. 
#
vault write pki/root/generate/internal \
      common_name=${ORG_CN} \
      ttl=8760h

#
# This is the PKI role which will be used to certify/sign the license-signing
# key exported from Transit
#
# FIXME: The use of 'allow_any_name' here is suboptimal but I found
# CN's and enforce_hostnames=false, allow_subdomains=true, etc to be a bit
# fiddly.
#
vault write pki/roles/license-signer \
      name='license-signer' \
      allowed-domans="${ORG_CN}" \
      allow_any_name=true \
      key_type=ec \
      key_bits=256

#
# Activate an instance of the Transit engine and create an exportable key to be
# used for licensing signing.
#
vault secrets enable transit
vault write -f transit/keys/licensing-key \
      exportable=true \
      type=ecdsa-p256

#
# Demonstrate the ability to read the exportable key, as a signing-key, and
# then extract it to a temporary file in PEM format.
# 
vault read transit/export/signing-key/licensing-key
vault read -format=json transit/export/signing-key/licensing-key
vault read -format=json transit/export/signing-key/licensing-key | jq -r '.data.keys[]' > signing-key.pem

#
# Wrap the exported signing key in a CSR.
#
openssl req -new -sha256 \
	-subj="/C=US/ST=CA/L=San Jose/O=${ORG_CN}/OU=licensing/CN=${ORG_CN}" \
	-key signing-key.pem \
	-out signing-key.csr

#
# Sign the CSR with the Vault programmable CA.
#
vault write pki/sign/license-signer \
      common_name="license-signer.${ORG_CN}" \
      ttl=720h \
      format=pem \
      csr=@signing-key.csr
      

