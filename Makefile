all: doc

doc:
	@echo ""
	@echo "**********************************************"
	@echo "***** Building Markdown doc from Org ... *****"
	@echo "**********************************************"
	pandoc -s --toc -f org -t commonmark -o README.markdown README.org
